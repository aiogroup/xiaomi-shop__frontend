$(document).ready(function() {

    $('.product-cart__detail-left-content').each(function() {
      var size = 23,
            blogContent = $(this).find('.product-cart__detail-left-content__product-title'),
            blogText = blogContent.html();
    if (blogText.length > size) {
        blogContent.html(blogText.slice(0, size) + '...');
    }});

    $('.product-left-content__in-category').each(function() {
      var size = 12,
            blogContent = $(this).find('.product-title__in-category'),
            blogText = blogContent.html();
    if (blogText.length > size) {
        blogContent.html(blogText.slice(0, size) + '...');
    }});


    $('.search-icon').click(function() {
      $('.search').slideToggle("fast");
    });

    $('.product-detail__page__product-image__carousel').slick({
      arrows: false,
      dots: true
    });

    $('.product-detail__page__product-image__carousel').magnificPopup({
      delegate: 'a',
      type: 'image',
      gallery: {
        enabled: true, 
        preload: [0,2],
        navigateByImgClick: true,
        arrowMarkup: '<button title="%title%" type="button" class="mfp-arrow mfp-arrow-%dir%"></button>',
        tPrev: 'Previous (Left arrow key)',
        tNext: 'Next (Right arrow key)',
        tCounter: '<span class="mfp-counter">%curr% of %total%</span>'
      }
    });
    
});


$('.section-recomended-products__tab-header__tab-trigger').hover(function() {
    var id = $(this).attr('data-tab'),
    content = $('.section-recomended-products__tab-content[data-tab="'+ id +'"]');
    
    $('.section-recomended-products__tab-header__tab-trigger.active').removeClass('active');
    $(this).addClass('active');
    
    $('.section-recomended-products__tab-content.active').removeClass('active');
    content.addClass('active');
 });










